package com.nana.marketplate.catalog.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.nana.marketplate.catalog.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, String> {

}
